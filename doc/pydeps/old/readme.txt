on target:

pydeps ./usr/lib/python3.7/site-packages/tcpclientserver --show-dot --noshow --pylib

on host:

copy over the result to host as tcpclientserver.dot

dot -Tsvg tcpclientserver.dot -o tcpclientserver.svg
